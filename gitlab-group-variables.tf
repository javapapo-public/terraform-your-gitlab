resource "gitlab_group_variable" "my_blog_url" {
  group = gitlab_group.public.id
  key = "my_custom_url"
  value = "http://javapapo.blogspot.com"
  protected = false
  masked = false
}

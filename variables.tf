variable "region" {
  default = "eu-central-1"
}

variable "gitlab_token" {
}

variable "renovate-bot-id" {
  default = "1263493"
}

variable "my-friend-id" {
  default = "2443231"
}
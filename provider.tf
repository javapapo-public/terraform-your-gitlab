provider "aws" {
  region = var.region
}

provider "gitlab" {
  token = var.gitlab_token
}
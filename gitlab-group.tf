resource "gitlab_group" "public" {
  name = "public"
  path = "public"
  visibility_level = "public"
  description = "My public repositories"
}

resource "gitlab_group" "private" {
  name = "private"
  path = "private"
  visibility_level = "private"
  description = "My private repositories"
}




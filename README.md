## gitlab terraform
Sample project for [this](http://javapapo.blogspot.com/2020/10/terraforming-my-gitlab-projects-on.html) blog post.

Assuming you have already `aws cli /env` configured to the correct region + the correct role.

### init
```bash
tf init -backend-config="state.config"
```

### plan

```bash
tf plan -var="gitlab_token=XXXXXX"
```

### apply

```bash
tf apply -var="gitlab_token=XXXXXX"
```


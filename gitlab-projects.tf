//------------------------------- PUBLIC REPO---------------------------------------------------------------------------
resource "gitlab_project" "my-public-repo" {
  name = "pukie-bot"
  path = "pukie-bot"
  visibility_level = "public"
  namespace_id = gitlab_group.private.id
  description = "my-public-repo"
  shared_runners_enabled = true
  issues_enabled = false
  merge_requests_enabled = true
  remove_source_branch_after_merge = true
}

resource "gitlab_project_membership" "renovate-bot" {
  project_id = gitlab_project.my-public-repo.id
  user_id = var.renovate-bot-id
  access_level = "developer"

}
resource "gitlab_project_membership" "myfriend" {
  project_id = gitlab_project.my-public-repo.id
  user_id = var.my-friend-id
  access_level = "developer"
}

resource "gitlab_project_variable" "my-custom-variable" {
  project = gitlab_project.my-public-repo.id
  key = "my-custom-variable"
  value = "my-custom-variable-value"
  protected = false
}

//------------------------------- PRIVATE REPO -------------------------------------------------------------------------
resource "gitlab_project" "my-private-repo" {
  name = "my-private-repo"
  path = "my-private-repo"
  visibility_level = "private"
  namespace_id = gitlab_group.private.id
  description = "My private stuff"
  shared_runners_enabled = false
  issues_enabled = false
  merge_requests_enabled = false
  remove_source_branch_after_merge = true
  container_registry_enabled = false
  wiki_enabled = false
}
